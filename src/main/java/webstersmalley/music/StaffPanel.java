package webstersmalley.music;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.util.Random;

/**
 * Created by: Matthew Smalley
 * Date: 06/06/13
 */
public class StaffPanel extends JPanel {

    private void drawLines(Graphics2D g2d, int startX, int startY, int endX) {
        for (int currentY = startY; currentY <= startY+40; currentY = currentY + 10) {
            g2d.drawLine(startX, currentY, endX, currentY);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        drawLines(g2d, 100, 10, 900);
        drawLines(g2d, 100, 70, 900);
        drawLines(g2d, 100, 130, 900);
        drawLines(g2d, 100, 190, 900);


    }

}
