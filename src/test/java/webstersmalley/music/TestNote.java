package webstersmalley.music;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by: Matthew Smalley
 * Date: 06/06/13
 */
public class TestNote {

    @Test
    public void testNotesFromE4() {
        Note note = Note.createNote(0, "C4", "Crotchet");

        assertEquals(28, note.getPitchNumber());
    }
}

