package webstersmalley.music;

import java.awt.Graphics2D;

/**
 * Created by: Matthew Smalley
 * Date: 06/06/13
 */
public class Crotchet extends Note {

    @Override
    public float getBeatLength() {
        return 1;
    }

    @Override
    protected void drawNote(Graphics2D graphics2D, int x, int y) {
        int[] coordinates = getNoteCoordinates(x, y);
        graphics2D.fillOval(coordinates[0], coordinates[1]-5, 15, 10);
        graphics2D.drawLine(coordinates[0]+15, coordinates[1], coordinates[0]+15, coordinates[1]-40);
    }
}
