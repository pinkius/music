package webstersmalley.music;

import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 06/06/13
 */
public abstract class Note {


    public abstract float getBeatLength();

    public enum DotType {
        NONE, SINGLE, DOUBLE, TRIPLE
    }

    private float beatNumber;
    private String name;
    private String pitch;
    private static Map<String, Integer> BASE_PITCH_NUMBERS;
    private static int NOTES_IN_AN_OCTIVE = 7;
    protected static final int PITCH_SPACING = -5;
    private DotType dotType;

    static {
        BASE_PITCH_NUMBERS = new HashMap<String, Integer>();
        BASE_PITCH_NUMBERS.put("C", 0);
        BASE_PITCH_NUMBERS.put("C#", 0);
        BASE_PITCH_NUMBERS.put("D", 1);
        BASE_PITCH_NUMBERS.put("D#", 1);
        BASE_PITCH_NUMBERS.put("E", 2);
        BASE_PITCH_NUMBERS.put("F", 3);
        BASE_PITCH_NUMBERS.put("F#", 3);
        BASE_PITCH_NUMBERS.put("G", 4);
        BASE_PITCH_NUMBERS.put("G#", 4);
        BASE_PITCH_NUMBERS.put("A", 5);
        BASE_PITCH_NUMBERS.put("A#", 5);
        BASE_PITCH_NUMBERS.put("B", 6);
    }

    protected abstract void drawNote(Graphics2D graphics2D, int x, int y);

    public final void draw(Graphics2D graphics2D, int x, int y) {
        drawNote(graphics2D, x, y);
        drawDots(graphics2D, x, y);
        drawLedgerLines(graphics2D, x, y);
    }

    public final void drawDots(Graphics2D graphics2D, int x, int y) {
        if (dotType == DotType.SINGLE) {
            int dotY = y + PITCH_SPACING * notesFromE4();
            int i = notesFromE4();
            if (i % 2 == 0) {
                dotY -= 6;
            }
            graphics2D.fillOval(x + 18, dotY, 5, 5);
        }
    }

    public final void drawLedgerLines(Graphics2D graphics2D, int x, int y) {
        int startX = x - 5;
        int endX = startX + 25;
        int i = notesFromE4();
        if (i < -1) {
            for (int j = 1; j <= i / -2; j++) {
                int lineY = y - (j * PITCH_SPACING * 2);
                graphics2D.drawLine(startX, lineY, endX, lineY);
            }
        }

        i = notesFromF5();
        if (i > 1) {
            for (int j = 1; j <= i / 2; j++) {
                int lineY = y + PITCH_SPACING * 8 + (j * PITCH_SPACING * 2);
                graphics2D.drawLine(startX, lineY, endX, lineY);
            }
        }
    }

    protected int[] getNoteCoordinates(int baseX, int baseY) {
        int[] coordinates = new int[2];
        coordinates[0] = baseX;
        coordinates[1] = baseY + notesFromE4() * PITCH_SPACING;
        return coordinates;
    }

    protected int getPitchNumber() {
        return getPitchNumber(this.getPitch());
    }

    protected int getPitchNumber(String pitch) {
        if (pitch == null) {
            throw new RuntimeException("Pitch should be set before calling this method.");
        }
        int octaveNumber = Integer.valueOf(pitch.substring(pitch.length() - 1));
        String baseNote = pitch.substring(0, pitch.length() - 1);
        int baseNoteNumber = BASE_PITCH_NUMBERS.get(baseNote);
        return baseNoteNumber + octaveNumber * NOTES_IN_AN_OCTIVE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPitch() {
        return pitch;
    }

    public void setPitch(String pitch) {
        this.pitch = pitch;
    }

    public float getBeatNumber() {
        return beatNumber;
    }

    public void setBeatNumber(float beatNumber) {
        this.beatNumber = beatNumber;
    }

    public static Note createNote(float beatNumber, String pitch, String type) {
        return createNote(beatNumber, pitch, type, DotType.NONE);
    }

    public static Note createNote(float beatNumber, String pitch, String type, DotType dotType) {
        try {
            Note note = (Note) Class.forName("webstersmalley.music." + type).newInstance();
            note.setBeatNumber(beatNumber);
            note.setPitch(pitch);
            note.setDotType(dotType);

            return note;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    protected int notesFromE4() {
        return getPitchNumber() - getPitchNumber("E4");
    }

    protected int notesFromF5() {
        return getPitchNumber() - getPitchNumber("F5");
    }

    public DotType getDotType() {
        return dotType;
    }

    public void setDotType(DotType dotType) {
        this.dotType = dotType;
    }
}
