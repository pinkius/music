package webstersmalley.music;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 06/06/13
 */
public class NoteGenerator {

    private void addNote(Map<Float, Set<Note>> notes, Note note) {
        Set<Note> notesForBeat = notes.get(note.getBeatNumber());
        if (notesForBeat == null) {
            notesForBeat = new HashSet<Note>();
            notes.put(note.getBeatNumber(), notesForBeat);
        }
        notesForBeat.add(note);
    }

    public Map<Float, Set<Note>> getNotes() {
        Map<Float, Set<Note>> notes = new HashMap<Float, Set<Note>>();
        int beatNumber = 0;
        addNote(notes, Note.createNote(1, "E4", "Crotchet"));
        addNote(notes, Note.createNote(2, "E4", "Crotchet"));
        addNote(notes, Note.createNote(3, "E4", "Minim"));
        addNote(notes, Note.createNote(5, "E4", "Crotchet"));
        addNote(notes, Note.createNote(6, "E4", "Crotchet"));
        addNote(notes, Note.createNote(7, "E4", "Minim"));
        addNote(notes, Note.createNote(9, "E4", "Crotchet"));
        addNote(notes, Note.createNote(10, "G4", "Crotchet"));
        addNote(notes, Note.createNote(11, "C4", "Crotchet", Note.DotType.SINGLE));

        addNote(notes, Note.createNote(12.5f, "D4", "Quaver"));
        addNote(notes, Note.createNote(13, "E4", "Semibreve"));

        addNote(notes, Note.createNote(17, "F4", "Crotchet"));
        addNote(notes, Note.createNote(18, "F4", "Crotchet"));
        addNote(notes, Note.createNote(19, "F4", "Crotchet", Note.DotType.SINGLE));
        addNote(notes, Note.createNote(20.5f, "F4", "Quaver"));
        addNote(notes, Note.createNote(21, "F4", "Crotchet"));
        addNote(notes, Note.createNote(22, "E4", "Crotchet"));
        addNote(notes, Note.createNote(23, "E4", "Crotchet"));
        addNote(notes, Note.createNote(24, "E4", "Quaver"));
        addNote(notes, Note.createNote(24.5f, "E4", "Quaver"));
        addNote(notes, Note.createNote(25, "E4", "Crotchet"));
        addNote(notes, Note.createNote(26, "D4", "Crotchet"));
        addNote(notes, Note.createNote(27, "D4", "Crotchet"));
        addNote(notes, Note.createNote(28, "E4", "Crotchet"));
        addNote(notes, Note.createNote(29, "D4", "Minim"));
        addNote(notes, Note.createNote(31, "G4", "Minim"));

        return notes;
    }
}
