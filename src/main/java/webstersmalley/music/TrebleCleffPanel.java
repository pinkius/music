package webstersmalley.music;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 06/06/13
 */
public class TrebleCleffPanel extends JPanel {
    protected static final int BEAT_SPACING = 30;
    protected static final int BAR_LINE_SPACING = 15;
    protected static final int MAXIMUM_X = 750;

    private void drawStaff(Graphics2D g2d, int startX, int startY, int endX) {
        try {
            g2d.drawImage(ImageIO.read(getClass().getResourceAsStream("/G_clef.png")), startX + 20, startY - 10, null);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        for (int currentY = startY; currentY <= startY + 40; currentY = currentY + 10) {
            g2d.drawLine(startX, currentY, endX, currentY);
        }
    }

    public TrebleCleffPanel() {
        setOpaque(false);
        setBackground(Color.WHITE);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);


        Graphics2D g2d = (Graphics2D) g;
        drawStaff(g2d, 100, 20, 900);
        drawStaff(g2d, 100, 120, 900);
        drawStaff(g2d, 100, 220, 900);
        drawStaff(g2d, 100, 320, 900);

        drawNotes(g2d);
    }

    private void drawNotes(Graphics2D g2d) {
        BasicStroke bs1 = new BasicStroke(2, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_ROUND, 1.0f, null, 2f);
        g2d.setStroke(bs1);

        int x = 150;
        int y = 60;
        NoteGenerator noteGenerator = new NoteGenerator();
        Map<Float, Set<Note>> notes = noteGenerator.getNotes();
        List<Float> beats = new ArrayList<Float>();
        beats.addAll(notes.keySet());
        Collections.sort(beats);

        int currentBar = 1;
        for (float f: beats) {
            if (f >= currentBar*4+1) {
                drawBar(g2d, x, y);
                x += BAR_LINE_SPACING;
                currentBar++;
                if (x > MAXIMUM_X) {
                    x = 150;
                    y += 100;
                }
            }
            Set<Note> notesAtBeat = notes.get(f);
            float maximumNoteLength = 1;
            if (notesAtBeat != null) {
                for (Note note: notesAtBeat) {
                    note.draw(g2d, x, y);
                    maximumNoteLength = Math.max(maximumNoteLength, note.getBeatLength());
                }
            }
            x += BEAT_SPACING * maximumNoteLength;
        }

    }

    private void drawBar(Graphics2D g2d, int x, int y) {
        g2d.drawLine(x, y, x, y-40);
    }

}
